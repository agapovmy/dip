#!/usr/bin/env python3
# -*- coding: utf-8 -*-
base_path = 'TsOI/Fruits/Training'

classes = [
    'AppleBraeburn',
    'Strawberry',
]
 
import glob
from skimage import io
from skimage.color import rgb2gray
 
data = {}
 
for im_class in classes:
    data[im_class] = []
    for file in glob.glob('{}/{}/*'.format(base_path, im_class)):
        data[im_class].append(
            rgb2gray(io.imread(file))
        )
        
import numpy as np
from skimage.feature import canny
from skimage.filters import roberts
from skimage.measure import find_contours
import random
from scipy.spatial import distance
 
from pywt import dwt2
 
dont_k = {}
ressS = {}

tp, fp, fn, tn = 0, 0, 0, 0 #t-true, f - false, p - positive, n - neg

r_countour_theshold = 0.1

for im_class in data:
    print('NOW', im_class)
    
    dont_k[im_class] = 0
    ressS[im_class] = []
   
    print (im_class)
    cnt_p_calss = 50
    for image in data[im_class]:
        
        #### TEST TEST TEST ####
        #contours = find_contours(canny(image), r_countour_theshold)
        contours = find_contours(image, r_countour_theshold)
        #### TEST TEST TEST ####
        
        points = set()
        for countour in contours:
            for points_ in countour:
                points.add(tuple(points_))
        choiced_cnt_pnt = []
        def calc_cnt_pnt(points, f):
            x, y = [], []
            for el in points:
                x.append(el[0])
                y.append(el[1])
            return (f(x), f(y))
        for choice in range(1, 100):
            choiced_cnt_pnt.append(calc_cnt_pnt(random.sample(points, 4), np.mean))
        cnt_point = calc_cnt_pnt(choiced_cnt_pnt, np.median)
       
        dists = []
        for point in points:
            dists.append(distance.euclidean(cnt_point, point))
       
        #### TEST TEST TEST ####
        #_, (cH, cV, cD) = dwt2(canny(image), 'haar')
        _, (cH, cV, cD) = dwt2(image, 'haar')
        #### TEST TEST TEST ####
        
        en = (cH**2 + cV**2 + cD**2).sum() / image.size
        print ('en =', en)
       
        my_max = np.percentile(dists, 75)
        my_min = np.percentile(dists, 10)
       
        for point in points:
            dst = distance.euclidean(cnt_point, point)
            if abs(dst - my_max) < .001:
                max_pnt = point
            if abs(dst - my_min) < .001:
                min_pnt = point
               
        print(float (my_max) / my_min)
        
        if en < 1:
            ressS[im_class].append(en)
        else:
            continue
        
        cnt_p_calss -= 1
        if cnt_p_calss == 0:
            break

import scipy.stats

def mean_confidence_interval(data, confidence=0.99999999):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h

#Определяем доверительный интервал двух выборок энергий изображения
_, leftApple, rightApple = mean_confidence_interval(ressS['AppleBraeburn']);
_, leftStraw, rightStraw = mean_confidence_interval(ressS['Strawberry']);

print ('Apple interval: {', leftApple, ',', rightApple, '}')
print ('Strawberry interval: {', leftStraw, ',', rightStraw, '}')

if rightApple > rightStraw :
    if leftApple < rightStraw :
        leftApple = rightStraw = (rightStraw - leftApple) / 2
elif rightApple < rightStraw :
    if leftStraw < rightApple :
        leftStraw = rightApple = (rightApple - leftStraw) / 2

for im_class in classes :
    for en in ressS[im_class]:
        if en >= leftStraw and en <= rightStraw:
            print ('Strawberry')
            if im_class =='Strawberry':
                tn += 1
            else:
                fn += 1
        elif en >= leftApple and en <= rightApple:
            print ('AppleBraeburn')
            if im_class =='AppleBraeburn':
                tp += 1
            else:
                fp += 1
        else:
            dont_k[im_class] += 1
 

import pandas as pd
pd.DataFrame(ressS).hist()

print (tp / (tp + fp), tn / (tn + fn), dont_k)

print ('TESTTESTTESTESTTEST')
base_path = 'TsOI/Fruits/Testing'

data = {}
 
for im_class in classes:
    data[im_class] = []
    for file in glob.glob('{}/{}/*'.format(base_path, im_class)):
        data[im_class].append(
            rgb2gray(io.imread(file))
        )

dont_k = {}
ressS = {}

tp, fp, fn, tn = 0,0,0,0

for im_class in data:
    #robs_dat[im_class] = []
    dont_k[im_class] = 0
    ressS[im_class] = []
   
    print (im_class)
    cnt_p_calss = 50
    for image in data[im_class]:
        
        #### TEST TEST TEST ####
        #contours = find_contours(canny(image), r_countour_theshold)
        contours = find_contours(image, r_countour_theshold)
        #### TEST TEST TEST ####
           
        points = set()
        for countour in contours:
            for points_ in countour:
                points.add(tuple(points_))
        choiced_cnt_pnt = []
        def calc_cnt_pnt(points, f):
            x, y = [], []
            for el in points:
                x.append(el[0])
                y.append(el[1])
            return (f(x), f(y))
        for choice in range(1, 100):
            choiced_cnt_pnt.append(calc_cnt_pnt(random.sample(points, 4), np.mean))
        cnt_point = calc_cnt_pnt(choiced_cnt_pnt, np.median)
       
        dists = []
        for point in points:
            dists.append(distance.euclidean(cnt_point, point))
       
        #### TEST TEST TEST ####
        #_, (cH, cV, cD) = dwt2(canny(image), 'haar')
        _, (cH, cV, cD) = dwt2(image, 'haar')
        #### TEST TEST TEST ####
        
        en = (cH**2 + cV**2 + cD**2).sum() / image.size
        print (en)
       
        my_max = np.percentile(dists, 75)
        my_min = np.percentile(dists, 10)
       
        for point in points:
            dst = distance.euclidean(cnt_point, point)
            if abs(dst - my_max) < .001:
                max_pnt = point
            if abs(dst - my_min) < .001:
                min_pnt = point
               
        print(float (my_max) / my_min)
        
        if en < 1:
            ressS[im_class].append(en)
        else:
            continue
           
        if en >= leftStraw and en <= rightStraw:
            print ('Strawberry')
            if im_class =='Strawberry':
                tn += 1
            else:
                fn += 1
        elif en >= leftApple and en <= rightApple:
            print ('AppleBraeburn')
            if im_class =='AppleBraeburn':
                tp += 1
            else:
                fp += 1
        else:
            dont_k[im_class] += 1
       
        cnt_p_calss -= 1
        if cnt_p_calss == 0:
            break
        
print (tp / (tp + fp), tn / (tn + fn), dont_k)
pd.DataFrame(ressS).hist()

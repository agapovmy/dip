#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 16:02:11 2019

@author: twist
"""

file = "Lab3/main/082.jpg"

from skimage import io
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
import numpy as np

test_img = io.imread(file)

grayscaled = rgb2gray(test_img)

h = len(grayscaled)
w = len(grayscaled[0])

winSize = 100;

for iw in range(0, h - winSize, 20) :
    for jw in range(0, w - winSize, 20) :
        sum1 = 0
        sum2 = 0
        for i in range(10, 90) :
            for j in range(10, 90) :
                if (i < 50) :
                    sum1 -= grayscaled[i + iw,j + jw]
                else:
                    sum1 += grayscaled[i + iw,j + jw]
                if (j < 50) :
                    sum2 -= grayscaled[i + iw,j + jw]
                else:
                    sum2 += grayscaled[i + iw,j + jw]
#        if (sum >= -400 and sum <= -300) or (sum <= 0 and sum >= -150) :
#        if sum >= -65 and sum <= 0:
#        if ((sum1 >= 295 and sum1 <= 490) or (sum1 <= 105 and sum1 >= 50)) and ((sum2 >=-65 and sum2 <= 0) or (sum2 <= -95 and sum2 >= -150)):
        if sum1 >= 295 and sum1 <= 490 and sum2 >=-65 and sum2 <= 0 :
            print(sum1)
            print(sum2)
            tmp = grayscaled[iw : iw + winSize, jw : jw + winSize]
            fig, ax = plt.subplots()
            ax.imshow(tmp, cmap=plt.cm.gray)
            plt.show()
            
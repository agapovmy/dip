#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 15:13:22 2019

@author: twist
"""
#file = "Lab3/test/home/test_horisontal_"

from skimage import io
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
import numpy as np

sums = []

for file_num in range(1, 7) :
    
    file = 'Lab3/test/home/test_horisontal_' + str(file_num) + '.png'
    
    test_img = io.imread(file)
    
    grayscaled = rgb2gray(test_img)
    #grayscaled[50,:] = np.zeros(100)
    
    winSize = 100;
    
    #First index is row, second is column
    
    sum = 0
    
    for i in range(10, 90) :
        for j in range(10, 90) :
            if (i < 50) :
                sum -= grayscaled[i,j]
            else:
                sum += grayscaled[i,j]
    
    print(sum)
    fig, ax = plt.subplots()
    ax.imshow(grayscaled, cmap=plt.cm.gray)
    plt.show()
    sums.append(sum)
    
    #haar values:
    #   First interval [-400;-300]
    #   Second interval: [-150;0]
    
fig, ax = plt.subplots();
ax.hist(sums)
plt.show()
